<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Thanh Hai</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

  <link rel="stylesheet" href="assets/css/main.css" />
  <link rel="stylesheet" href="assets/css/responsive.css" />
  <!-- <link rel="stylesheet" href="css/light_template.css" /> -->
  <link rel="stylesheet" href="assets/css/colors/red-color.css" />
  
  <!-- Plugins -->
  <link rel="stylesheet" href="assets/css/magnific-popup.css" />

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <!-- google Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script|Poppins:300i,400,600" />
    <!-- Latest compiled and minified CSS -->

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script >
            // Ajax function
            
            function _(id) {

                return document.getElementById(id);

            }

            function submitForm() {

                _("btn").disabled = true;
                _("status").innerHTML = 'please wait ...';

                var formdata = new FormData(),
                    ajax = new XMLHttpRequest();

                formdata.append("name", _("name").value);
                formdata.append("email", _("email").value);
                formdata.append("phone", _("phone").value);
                formdata.append("subject", _("subject").value);
                formdata.append("msg", _("msg").value);

                ajax.open("POST", "contact.php");
                ajax.onreadystatechange = function () {

                    if (ajax.readyState === 4 && ajax.status === 200) {

                        if (ajax.responseText === "success") {

                            _("my_form").innerHTML = '<h2>Thanks ' + _("name").value + ',<br> <br> your message has been sent.</h2>';

                        } else {

                            _("status").innerHTML = ajax.responseText;
                            _("btn").disabled = false;

                        }
                    }
                };

                ajax.send(formdata);
            }
        </script>
</head>
<body>

  <app-root></app-root>
        
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/typed.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custom.js"></script>

    <script type="text/javascript" src="js/app/runtime.js"></script>
    <script type="text/javascript" src="js/app/polyfills.js"></script>
    <script type="text/javascript" src="js/app/styles.js"></script>
    <script type="text/javascript" src="js/app/vendor.js"></script>
    <script type="text/javascript" src="js/app/main.js"></script>
</body>
</html>
